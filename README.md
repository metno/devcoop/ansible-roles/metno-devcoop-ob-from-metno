# Description

Ansible role to copy BUFR files from MET Norway.

The location of the ssh key for accessing the files must be specified
in the variable `devcoopmetno_sshkey_path`. The username must be
specified in the variable `devcoopmetno_user`. Both are chosen by MET
Norway.

Despite of its name, the role can also confiure nwp downloads from MET
Norway.

Setting up this role will not download anything unless agreed with MET
Norway.
